const fs = require("fs")
const path = require("path")
//  1. Retrieve data for ids : [2, 13, 23].

function retrieveData(ids) {
    return new Promise((resolve, reject) => {
        return fs.readFile(path.join(__dirname, "data.json"), ((err, data) => {
            if (err) {
                reject(err)
                return
            } else {
                let userData = JSON.parse(data)

                let employeesData = userData.employees.filter(data => {
                    return data.id == ids
                })
                employeesData = JSON.stringify(employeesData)
                fs.writeFile(path.join(__dirname, "filteredIds.json"), employeesData, ((err, data) => {
                    if (err) {
                        reject(err)
                        return
                    } else {
                        resolve(userData)
                    }
                }))
            }
        }))
    })

}
// retrieveData(2)
function groupDataBasedOnCompanies(userData) {
    // 
    return new Promise((resolve, reject) => {
        let employeesGroupedOnCompanies = userData.employees.reduce((accumulator, current) => {

            let arr = []
            if (accumulator[current.company]) {
                accumulator[current.company].push(current.name)
            } else {
                accumulator[current.company] = arr
            }
            return accumulator
        }, {})
        employeesGroupedOnCompanies = JSON.stringify(employeesGroupedOnCompanies)
        // console.log("vhf")
        fs.writeFile(path.join(__dirname, "groupedBasedOnCompany.json"), employeesGroupedOnCompanies, ((err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(userData)
            }
        }))
    })
}
// groupDataBasedOnCompanies()

function getAllDataofPowerPuff(userData, companyName) {
    return new Promise((resolve, reject) => {

        let dataForPowerPuffCompany = userData.employees.filter(data => {
            return data.company.includes(companyName)
        })
        dataForPowerPuffCompany = JSON.stringify(dataForPowerPuffCompany)

        fs.writeFile(path.join(__dirname, "dataForPowerPuff.json"), dataForPowerPuffCompany, ((err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(userData)
            }
        }))

    })


}

// getAllDataofPowerPuff("Powerpuff Brigade")



function removeId(userData, ids) {
    return new Promise((resolve, reject) => {
        // console.log("hj")
        let removeIdwithtwo = userData.employees.filter(data => {
            return data.id != ids
        })
        removeIdwithtwo = JSON.stringify(removeIdwithtwo)
        fs.writeFile(path.join(__dirname, "removedData.json"), removeIdwithtwo, ((err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(userData)
            }
        }))
    })


}

// removeId(2)

function sortBasedOnComapnyName(userData) {
    return new Promise((resolve, reject) => {
        let sortBasedOnCompany = userData.employees.sort((companyNameA, companyNameB) => {
            if (companyNameB.company > companyNameA.company) {
                return -1
            } else if (companyNameA.id == companyNameB.id) {
                if (companyNameB.id > companyNameA.id) {
                    return -1
                }
            }
        })
        sortBasedOnCompany = JSON.stringify(sortBasedOnCompany)
        fs.writeFile(path.join(__dirname, "sortBasedOnCompany.json"), sortBasedOnCompany, ((err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(userData)
            }
        }))

    })


}

// sortBasedOnComapnyName()
function swapCompany(userData, swap1, swap2) {
    return new Promise((resolve, reject) => {
        let resultJSON = userData
        let swap1data;
        let swap2data;
        for (let index = 0; index < resultJSON.length; index++) {
            if (resultJSON.employees[index]["id"] === swap1) {
                swap1 = index
                swap1data = resultJSON[index];
            } else if (resultJSON.employees[index]["id"] === swap2) {
                swap2 = index
                swap2data = resultJSON.employees[index];
            }


        }

        resultJSON.employees.slice(swap1, swap1, swap2data);
        resultJSON.employees.slice(swap2, swap2, swap1data);

        resultJSON = JSON.stringify(resultJSON)
        fs.writeFile(path.join(__dirname, "swapCompany.json"), resultJSON, (err) => {
            if (err) {
                reject(err)
            } else {

                resolve(userData)
            }
        })

    })
}


// swapCompany(93, 92)

function addBirthdayIfIdIsEven(userData) {

    return new Promise((resolve, reject) => {
        let addBirthKey = userData.employees.map(data => {
            let date = Date()
            if (data.id % 2 == 0) {
                data["birthday"] = date.split(' ').slice(1, 4)
            }
            return data
        })
        fs.writeFile(path.join(__dirname, "addBirthdayIfIdIsEven.json"), JSON.stringify(addBirthKey), ((err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(userData)
            }
        }))

    })

}


// addBirthdayIfIdIsEven()

module.exports.retrieve = retrieveData
module.exports.groupDataBasedOnCompany = groupDataBasedOnCompanies
module.exports.getAllDataofPowerPuf = getAllDataofPowerPuff
module.exports.removeId = removeId
module.exports.sortBasedOnComapnyNam = sortBasedOnComapnyName
module.exports.swapCompanye = swapCompany
module.exports.addBirthdayIfIdIsEvene = addBirthdayIfIdIsEven
